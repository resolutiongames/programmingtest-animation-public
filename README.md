# Animation programming assignment

It's really awesome of you to apply for a position at Resolution Games!

As part of the evaluation process, we let candidates make a small task in Unity.

Please read the following instructions *carfully* before starting to work on it.
Read them again before submitting.

## Preparations

The very first thing you should do is read through these instructions.
After that, send an e-mail to the email mentioned in the invite, with your suggested deadline for submission.

This means you choose your own deadline. There is no need to ask us if the deadline is OK.
After that, start working on the task and submit it no later than the deadline.

## Instructions

Make the squirl model in the Unity project come alive using the given animations. 

See [the movie](https://drive.google.com/file/d/1jFwm84fqdmVKSoL8XeHhS18lJL2uuDix/view?usp=sharing) 
to get an idea of what it can look like.

### Unity version

Make sure the Unity version you use matches the one in the project.
You can see that in Unity Hub, or by looking in the file `ProjectSettings/ProjectVersion.txt`.

### Game mechanics

The squirl should be able to do the following:

* Run forward using up the arrow. 
* Turn using the left and right arrow. 
* Jump using the space button (it should be able to jump up to, and fall down from, the platform in the main scene). 

The squirl should be a fast and nimble character, so keep that in mind while implemeting the movement. 
The controls should be "tight" and responsive, and the animations should reflect the player input.
Dont add any more features than this. 

### Version control

Start with cloning this repository. Keep it local. Do not create a public repository!
Work with Git locally as usual. Do not push, as you have no write permissions.
If you haven't worked with Git before, this is an opportunity to learn it.

### Other guidelines

* Make it simple, but write the code to a quality that's good enough for professional developers to continue working on. As a performance guideline for the code, design it so it runs well on relatively limited devices such as mobiles.

* It is important that your work is easily usable and tweakable by people from other disciplines (animators, game designers etc). Keep this in mind while structuring your work and the project. 

* Use the "Main" scene. 

* The model you should use is called `ZipLow` and is located in `Assets/Model`. It contains meshes for multiple skins. You can disable all skins except `ZipDefaultLowGeo`.

Given the guidelines above you are free to complete the task however you see fit. 


## Requirements

  * Follow Resolution's [Style guide](StyleGuide.pdf).
  * Don't add any new textures or models to the project. 
  * Don't use any third party libraries or components.

## Submission

Make sure you have committed the final version.

Then create a zip file of the .git directory.
The zip file should only contain the `.git` directory, nothing else.

Send the zip file to the suggested email (see your invite to the test).